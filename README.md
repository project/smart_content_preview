# Smart Content Preview

The Smart Content Preview module works with the Smart Content module. When adding or editing a segment set, this module will add a Preview checkbox next to each segment. If checked, that segment will always result in a true condition, regardless of whether or not the conditions are met. This can be used for previewing segment results or for testing purposes.
