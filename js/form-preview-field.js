/**
 * @file
 * Handle preview functionality on smart segment form.
 */
 (function ($, Drupal, once) {
  Drupal.behaviors.previewFieldBehavior = {
    attach: function (context, settings) {
      // Segment elements.
      let segments = $('.smart-content-segment-set-edit-form .smart-segments-preview');

      segments.each(function() {
        // Add click once for each segment.
        once('previewFieldBehavior', $(this)).forEach(function (element) {
            $(element).on('click', function() {
            // If checked, uncheck all the other checkboxes.
            if (this.checked) {
              segments.not(this).prop('checked', false);
            }
          });
        });
      });
    }
  };

})(jQuery, Drupal, once);